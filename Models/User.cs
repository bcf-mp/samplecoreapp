using System.ComponentModel.DataAnnotations;

namespace samplecoreapp.Models
{
    public class User
    {
        [Key]
        public int user_id { get; set; }
        public string login { get; set; }
        public string password { get; set; }
    }
}