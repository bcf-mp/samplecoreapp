using Microsoft.EntityFrameworkCore;
using samplecoreapp.Models;

namespace samplecoreapp.Models
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<User> User { get; set; }
        public DbSet<Items> Items { get; set; }
    }
}