using System.ComponentModel.DataAnnotations;

namespace samplecoreapp.Models
{
    public class Items
    {
        [Key]
        public int item_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }
    
}