# Baza

.open baza.db
.mode insert
.read /ścieżka/do/baza_dump.sql


# Git 

##### Config identity
```
git config --global user.name "John Doe"
git config --global user.email johndoe@example.com
```
##### Clone repository
```
git clone git@repo.org

git clone git@bitbucket.org:bcf-mp/samplecoreapp.git
```
##### Change branch
```
git checkout  -b XXX 
```
##### Check repo status
```
git status
```
##### Check differencess
```
git diff
```
##### Add new files to repo
```
git add file.cs
git add -A
```
##### Commit changes
```
git commit file.a file.b -m "Changes"
git commit -a -m "Changes"
```
##### Push changes to repo
```
git push
```
##### Download changes
```
git pull
git fetch
```
##### Merge branches/changes
```
git merge branch
```

# Jira
https://id.atlassian.com/login

