﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using samplecoreapp.Models;

namespace samplecoreapp.Controllers
{

    public class HomeController : Controller
    {
        private readonly DatabaseContext context;

        public HomeController(DatabaseContext context)
        {
             this.context = context;   
        }

        public IActionResult Index()
        {
            var rnd= new Random();
            byte[] ba = {0};
            ba[0]= (byte)rnd.Next(1000);
            HttpContext.Session.SetString("WasInIndex", "Token");
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
            ViewBag.sessionv=HttpContext.Session.GetString("WasInIndex");
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Login()
        {
            return View();
        }

        public IActionResult Items()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        public string ping(string msg)
        {
            return "pong";
        }

        public string Test()
        {
            var list = context.User.ToList();
            return list.ToString();
        }

        [HttpPost]
        public JsonResult UserAuth([FromBody]LoginCLass data)
        {
            ResultClass t = new ResultClass();
            t.type = 0; // 0 = ok
            t.msg = "message";
            return Json(t);
        }

        public class LoginCLass  
        {
            public string login { get; set; }
            public string password { get; set; }
        }

        public class ResultClass  
        {
            public int type { get; set; }
            public string msg { get; set; }  
        }
    }
}
